import React, {Component} from 'react';
import {Button, Form, FormGroup, FormText, Input, Label, Table} from "reactstrap";

class FormComponent extends Component {

    state = {
        students: [],

        student: {
            email: '',
            password: '',
            hobbies: [],
            level: '',
            gpa: 0,
            majors: ''
        }
    }

    handleChange = (event) => {
        console.log(event.target.name, event.target.value)
        this.setState(prevState => ({
            student: {
                ...prevState.student,
                [event.target.name]: event.target.value
            }
        }))
    }

    handleCheckbox = (event, value) => {
        let {students, student} = this.state

        let st = [...student.hobbies]
        if (st.includes(value)) {
            st = st.filter((item) => {
                return item !== value
            })
        } else {
            st.push(value)
        }

        this.setState(prevState => ({
            student: {
                ...prevState.student,
                hobbies: st
            }
        }))

        console.log(st)
    }


    saveStudent = () => {
        let {student, students} = this.state
        this.setState(prevState => ({
            ...prevState,
            students: [
                ...prevState.students, student
            ]
        }))
    }

    handleSubmit(event) {
        event.preventDefault();
    }


    render() {

        const {students} = this.state

        return (
            <div className={'container'}>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label for="email"><h5>Email</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Input type="email"
                                       required="required"
                                       name="email"
                                       id="email"
                                       value={this.state.student.email}
                                       onChange={this.handleChange}
                                       placeholder="My email..."/>
                                <span
                                    className={'mt-2 d-inline-block'}>Please enter your email.(required)</span>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label for="password"><h5>Password</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Input type="password"
                                       required="required"
                                       name="password"
                                       id="password"
                                       value={this.state.student.password}
                                       onChange={this.handleChange}/>
                                <span className={'mt-2 d-inline-block'}>Please enter your password.(required)</span>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup tag="fieldset" className="row mt-5">
                        <div className="col-md-2 text-center">
                            <h5>Hobbies</h5>
                        </div>
                        <div className="col-md-10 px-4 mt-2">
                            <FormGroup>
                                <div className="row">
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="checkbox"
                                                   name="hobbies"
                                                   value='Surfing'
                                                   checked={this.state.student.hobbies.includes('Surfing')}
                                                   onChange={(e) => this.handleCheckbox(e, 'Surfing')}
                                            />{' '}
                                            <h6>Surfing</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="checkbox"
                                                   name="hobbies"
                                                   value='Running'
                                                   checked={this.state.student.hobbies.includes('Running')}
                                                   onChange={(e) => this.handleCheckbox(e, 'Running')}
                                            />{' '}
                                            <h6>Running</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="checkbox"
                                                   name="hobbies"
                                                   value='Biking'
                                                   checked={this.state.student.hobbies.includes('Biking')}
                                                   onChange={(e) => this.handleCheckbox(e, 'Biking')}
                                            />{' '}
                                            <h6>Biking</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="checkbox"
                                                   name="hobbies"
                                                   value='Paddling'
                                                   checked={this.state.student.hobbies.includes('Paddling')}
                                                   onChange={(e) => this.handleCheckbox(e, 'Paddling')}
                                            />{' '}
                                            <h6>Paddling</h6>
                                        </Label>
                                    </div>
                                </div>
                            </FormGroup>
                        </div>


                    </FormGroup>
                    <FormGroup tag="fieldset" className="row mt-5">
                        <div className="col-md-2 text-center">
                            <h5>Level</h5>
                        </div>
                        <div className="col-md-10 px-4 mt-2">
                            <FormGroup>
                                <div className="row">
                                    <div className="col-md-2">
                                        <Label>
                                            <Input
                                                type="radio"
                                                name="level"
                                                value='freshman'
                                                checked={this.state.student.level === 'freshman'}
                                                onChange={this.handleChange}
                                            />{' '}
                                            <h6>Freshman</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="radio"
                                                   name="level"
                                                   value='sophomore'
                                                   checked={this.state.student.level === 'sophomore'}
                                                   onChange={this.handleChange}
                                            />{' '}
                                            <h6>Sophomore</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="radio"
                                                   name="level"
                                                   value='junior'
                                                   checked={this.state.student.level === 'junior'}
                                                   onChange={this.handleChange}
                                            />{' '}
                                            <h6>Junior</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="radio"
                                                   name="level"
                                                   value='middle'
                                                   checked={this.state.student.level === 'middle'}
                                                   onChange={this.handleChange}
                                            />{' '}
                                            <h6>Middle</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input type="radio"
                                                   name="level"
                                                   value='senior'
                                                   checked={this.state.student.level === 'senior'}
                                                   onChange={this.handleChange}
                                            />{' '}
                                            <h6>Senior</h6>
                                        </Label>
                                    </div>
                                    <div className="col-md-2">
                                        <Label>
                                            <Input
                                                type="radio"
                                                name="level"
                                                value='other'
                                                checked={this.state.student.level === 'other'}
                                                onChange={this.handleChange}
                                            />{' '}
                                            <h6>Other</h6>
                                        </Label>
                                    </div>
                                </div>
                            </FormGroup>
                        </div>


                    </FormGroup>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label for="gpa"><h5>GPA</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Input type="number"
                                       required="required"
                                       name="gpa"
                                       id="gpa"
                                       value={this.state.student.gpa}
                                       onChange={this.handleChange}
                                       placeholder="Please select a value"/>
                                <span
                                    className={'mt-2 d-inline-block'}>Select your GPA.(required)</span>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label for="exampleText"><h5>Majors</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Input type="textarea"
                                       name="majors"
                                       id="exampleText"
                                       value={this.state.student.majors}
                                       onChange={this.handleChange}/>
                                <span
                                    className={'mt-2 d-inline-block'}>Please enter your first name and last name.(required)</span>
                            </div>
                        </div>
                    </FormGroup>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label for="exampleFile"><h5>File</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Input type="file" name="file" id="exampleFile"/>
                                <FormText color="muted">
                                    Please, send a profile picture to us!
                                </FormText>
                            </div>
                        </div>


                    </FormGroup>
                    <FormGroup>
                        <div className="row mt-2">
                            <div className="col-md-2 text-center">
                                <Label><h5>Submit</h5></Label>
                            </div>
                            <div className="col-md-10">
                                <Button className="ml-1 btn-success"
                                        onClick={this.saveStudent}>Submit</Button>
                            </div>
                        </div>
                    </FormGroup>
                </Form>
                <Table bordered className='mt-5 bg-light'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Hobbies</th>
                        <th>Level</th>
                        <th>GPA</th>
                        <th>Majors</th>
                    </tr>
                    </thead>
                    <tbody>
                    {students.map((item, index) => <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.email}</td>
                        <td>{item.password}</td>
                        <td>{item.hobbies.map((hobby, index) => <p key={index}>
                            {hobby}
                        </p>)}</td>
                        <td>{item.level}</td>
                        <td>{item.gpa}</td>
                        <td>{item.majors}</td>
                    </tr>)}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default FormComponent;