import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import FormComponent from "./components/FormComponent";

class App extends Component {

    state = {}

    render() {
        return <div className={'container border mt-5 p-4'} style={{backgroundColor: '#F5F5F5'}}>
            <FormComponent/>
        </div>
    }
}

export default App

ReactDOM.render(
    <App/>, document.getElementById('root')
)